from setuptools import setup, find_namespace_packages

setup(
    name='rite_vs2',
    version='0.0.3',
    include_package_data=True,
    packages=find_namespace_packages(where="src"),
    package_dir={"": "src"},
    package_data={
        "rite_vs2.detector_configs.config_C07-W0337": ["*.txt","*.xml"],
        "rite_vs2.detector_configs.config_E04-W0332": ["*.txt","*.xml"],
        "rite_vs2.detector_configs.config_J08-W0281": ["*.txt","*.xml"],
        "rite_vs2.detector_configs.": ["*.txt","*.xml"],
    }
)
