#!/usr/bin/env python

import os
from shutil import copyfile

chunk =	8
storage_id = 2
host = 16

def dk_get(host, storage_id, chunk):
    # setup vcom script: vcom dk get
    os.system("echo 'rt load /tmp/vcom8' > /tmp/get_sc.vc")
    os.system("echo 'dk get %d %d %d' >> /tmp/get_sc.vc" % (host, storage_id, chunk,))
    os.system("echo 'exit' >> /tmp/get_sc.vc")
    # run: vcom dk get
    os.system("~/zdenek/vcom-util/build/vcom -a 8 -p 192.168.128.118 < /tmp/get_sc.vc")
    # copy data
    copyfile("/tmp/vcom-util.bin", "/home/vzlusta2/zdenek/recon/dk-chunks/%03d-%04d-%06d" % (host, storage_id, chunk,))
    

for chunk in range(700,701):
    dk_get(host, 3, chunk)


