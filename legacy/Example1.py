#!/usr/bin/env python
# coding: utf-8

# In[1]:

# In[2]:


import numpy as np

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.colors as colors

import rite_vs2
import rite_vs2.decoder as rdc
import rite_vs2.utils as rutils


# In[3]:


# chunk/packets processors
metada_proc = rdc.metadada_processor()
data_proc = rdc.datastream_processor()

# chnk import function
read_chunk = rdc.read_chunk_v1

# DK storage ports
port_META = rite_vs2.DK_PORT_META
port_DATA = rite_vs2.DK_PORT_DATA


# In[4]:


# METADATA chunk

#name = "/home/vzlusta2/zdenek/rite-simul/dk-chunks-Si-2020-09-06-d"
#name = "/home/vzlusta2/zdenek/rite-simul/vcom-dump/dk-x"
#name = "/home/vzlusta2/recon/dk-y"
name = "/home/vzlusta2/zdenek/rite-simul/vcom-dump/chunks-2020-09-09"
#name = "dk-chunks-24"
#name = "/tmp/roi-1"

meta_chunk_id = 37
#meta_chunk_id = 1

print("name: %s\nmeta_chunk_id = %d\n" % (name, meta_chunk_id,))

chunk_meta = read_chunk(name, port_META, meta_chunk_id)

if chunk_meta is not None:
    pck_info = metada_proc.process(chunk_meta)
    rdc.print_metadata(pck_info)


# In[5]:


# DATA chunks processing loop

data_proc.reset() # same processor can be reused for different measurements

for k in range(pck_info['first_chunk'], pck_info['last_chunk']+1):
    chunk_pck = read_chunk(name, port_DATA, k)
    
    if chunk_pck is None:
        print("ERROR: Could not read chunk_id=%d" % k)
        continue
    
    data_proc.process(chunk_pck)

# print some data overview    
data_proc.print_info()

# get result
data = data_proc.data

# show typical access to data items, note: some of them may be still lists of arrays
ehist = data['ehist']
bin8 = data['bin8']
bin16 = data['bin16']
bin32 = data['bin32']
filt = data['filt']
frame = data['frame']
col_proj = data['col_proj']
row_proj = data['row_proj']


# In[6]:


# plot frame

if frame is not None:
    plt.figure()
    img = frame
    plt.subplot(111), plt.title('frame 0x%04x.%d (min=%d,max=%d), mode=%d' % (pck_info['frame_id'], pck_info['exp_nb'], 
                                                                              np.min(img[img>0]),np.max(img),pck_info['meas_mode'],))
    imgplot = plt.imshow(img, interpolation='none', norm=colors.LogNorm(vmin=1, vmax=img.max()))
    cbar = plt.colorbar(extend='max')
    cbar.minorticks_on()
    plt.show()
else:
    print("No frame data")


# In[7]:


# plot filt

if filt is not None:
    plt.figure()
    img = filt
    plt.subplot(111), plt.title('filt 0x%04x.%d (min=%d,max=%d), mode=%d' % (pck_info['frame_id'], pck_info['exp_nb'], 
                                                                             np.min(img[img>0]),np.max(img),pck_info['meas_mode'],))
    imgplot = plt.imshow(img, interpolation='none', norm=colors.LogNorm(vmin=1, vmax=img.max()))
    cbar = plt.colorbar(extend='max')
    cbar.minorticks_on()
    plt.show()
else:
    print("No filt data")


# In[8]:


# plot ehist

plt.figure(figsize=(10, 10), dpi=100)

if ehist is not None:
    for iroi in range(rite_vs2.DET_NROIS):
        if ehist[iroi][0] is not None:
            plt.subplot(3,2,iroi+1), plt.title('ehist 0x%04x.%d roi=%d' % (pck_info['frame_id'], pck_info['exp_nb'], iroi,))
            edges = np.linspace(0,ehist[iroi][1],rite_vs2.DET_EHIST_NBINS+1)
            plt.bar(edges[:-1], ehist[iroi][0], width=np.diff(edges), edgecolor="black", align="edge")
    plt.show()
else:
    print("No ehist data")


# In[9]:


# plot bin8

if filt is not None:
    plt.figure()
    img = bin8
    plt.subplot(111), plt.title('bin8 0x%04x.%d (min=%d,max=%d), mode=%d' % (pck_info['frame_id'], pck_info['exp_nb'],
                                                                             np.min(img[img>0]),np.max(img),pck_info['meas_mode'],))
    imgplot = plt.imshow(img, interpolation='none', norm=colors.LogNorm(vmin=1, vmax=img.max()))
    cbar = plt.colorbar(extend='max')
    cbar.minorticks_on()
    plt.show()
else:
    print("No bin8 data")


# In[10]:


# plot bin16

if filt is not None:
    plt.figure()
    img = bin16
    plt.subplot(111), plt.title('bin16 0x%04x.%d (min=%d,max=%d), mode=%d' % (pck_info['frame_id'], pck_info['exp_nb'], 
                                                                              np.min(img[img>0]),np.max(img),pck_info['meas_mode'],))
    imgplot = plt.imshow(img, interpolation='none', norm=colors.LogNorm(vmin=1, vmax=img.max()))
    cbar = plt.colorbar(extend='max')
    cbar.minorticks_on()
    plt.show()
else:
    print("No bin16 data")


# In[11]:


# plot bin32

if filt is not None:
    plt.figure()
    img = bin32
    plt.subplot(111), plt.title('bin32 0x%04x.%d (min=%d,max=%d), mode=%d' % (pck_info['frame_id'], pck_info['exp_nb'], 
                                                                              np.min(img[img>0]),np.max(img),pck_info['meas_mode'],))
    imgplot = plt.imshow(img, interpolation='none', norm=colors.LogNorm(vmin=1, vmax=img.max()))
    cbar = plt.colorbar(extend='max')
    cbar.minorticks_on()
    plt.show()
else:
    print("No bin32 data")


# In[12]:


# plot col_proj

plt.figure(figsize=(12, 14), dpi=100)

if col_proj is not None:
    for iroi in range(rite_vs2.DET_NROIS):
        if col_proj[iroi] is not None:
            roi = pck_info['rois'][iroi]
            first = roi[1]
            last = roi[1]+roi[3]
            plt.subplot(3,2,iroi+1), plt.title('col_proj 0x%04x.%d roi=%d' % (pck_info['frame_id'], pck_info['exp_nb'], iroi,))
            x = np.linspace(first,last,last-first+1,endpoint=True)
            xx = np.zeros((2*(x.size-1),))
            xx[0::2] = x[:-1]
            xx[1::2] = x[1:]
            yy = np.zeros((2*(x.size-1),))
            yy[0::2] = col_proj[iroi]
            yy[1::2] = col_proj[iroi]
            plt.plot(xx, yy)
            plt.xlim((first,last))
            plt.xlabel('col')
            plt.ylabel('events')
    plt.show()
else:
    print("No col_proj data")


# In[13]:


# plot row_proj

plt.figure(figsize=(12, 14), dpi=100)

if row_proj is not None:
    for iroi in range(rite_vs2.DET_NROIS):
        if row_proj[iroi] is not None:
            roi = pck_info['rois'][iroi]
            first = roi[0]
            last = roi[0]+roi[2]
            plt.subplot(3,2,iroi+1), plt.title('row_proj 0x%04x.%d roi=%d' % (pck_info['frame_id'], pck_info['exp_nb'], iroi,))
            x = np.linspace(first,last,last-first+1,endpoint=True)
            xx = np.zeros((2*(x.size-1),))
            xx[0::2] = x[:-1]
            xx[1::2] = x[1:]
            yy = np.zeros((2*(x.size-1),))
            yy[0::2] = row_proj[iroi]
            yy[1::2] = row_proj[iroi]
            plt.plot(xx, yy)
            plt.xlim((first,last))
            plt.xlabel('row')
            plt.ylabel('events')
    plt.show()
else:
    print("No row_proj data")


# In[14]:


data['ehist'][0][0]

