# RITE recon

Reconstruction/decoding of DK data from RITE modelu for vzlusat2.

## Installation

```
git clone https://gitlab.com/vzlusat2/recon.git
cd recon
pip install .
```

## Example of unpacked metadata

```bash
[RITE] start_nb: 265
[MEAS] exp_nb: 5
[MEAS] frame_id: 2309
[MEAS] time: 2020-08-14 11:58:36
[DATA] outputForm: 63
[DATA] firstChunkId: 631
[DATA] lastChunkId: 700
[DET] mode: 1
[DET] scan mode, (count) threshold, roi: 0, 500, 0
[DET] frameTime: 10 (sec)
[DET] bias: 0 (V)
[DET] threshold: 5.01634 (keV)
[DET] nFrames: 1 
[PROC] filtering: 1 
[PROC] (frame) min, max, (filtered) min, max:          0         67          0         67
[PROC]    id  row  col  hgt  wdh  frame-evt   filt-evt  frame-sum   filt-sum
[PROC] roi0:    0    0  256  256         57          4       1689        207
[PROC] roi1:    0    0  128  128         24          1        720         62
[PROC] roi2:    0  128  128  128         26          3        776        145
[PROC] roi3:  128    0  128  128          5          0        147          0
[PROC] roi4:    0    0    0    0          0          0          0          0
[PROC] en_hist_limit: 67
```
