#!/usr/bin/env python

import numpy as np

# czlusat2 RITE CSP address
RITE_CSP_ADDRESS = 16
# DK metadat port
DK_PORT_META = 2
# DK data port
DK_PORT_DATA = 3

# vzlusat2 RITE Timepix detector parameters
DET_SZ = 256*256
DET_HEIGHT = 256
DET_WIDTH = 256

DET_DTYPE = np.uint16

# vzlusat2 RITE parameters
DET_NROIS = 5
DET_EHIST_NBINS = 16
DEFAULT_PACKET_LEN = 64

# import submodules
from . import decoder, utils, hexchunks