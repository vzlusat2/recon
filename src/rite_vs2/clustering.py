#!/usr/bin/env python

import numpy as np
from scipy import ndimage as ndi
from skimage.measure import regionprops

def filter_clusters_skim(img, clust_dsz_lim=1):

    # labeling structure (feature connections)
    s = [[1,1,1],
         [1,1,1],
         [1,1,1]]

    # label image features
    labels, nfeatures = ndi.label(img, structure=s)
    
    # calculate region properties
    props = regionprops(labels)
    
    # calculate vertical and horizontal dimensions
    hsz = np.zeros((len(props),), np.int32)
    vsz = np.zeros((len(props),), np.int32)

    for i, _props in enumerate(props):
        vsz[i] = _props.bbox[2] - _props.bbox[0]
        hsz[i] = _props.bbox[3] - _props.bbox[1]
        
    filt_idx = np.argwhere(np.logical_and(vsz<=clust_dsz_lim,hsz<=clust_dsz_lim)).flatten()
    
    img_filt = np.zeros(img.shape, img.dtype)
    
    # copy filtered events to a new image
    for i in filt_idx:
        lidx = labels==props[i].label
        img_filt[lidx] = img[lidx]
        
    return img_filt, filt_idx.size
