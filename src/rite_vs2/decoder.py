#!/usr/bin/env/python

import struct, datetime, pytz
import numpy as np
import os

from . import DET_SZ, DET_HEIGHT, DET_WIDTH, DET_DTYPE, DET_NROIS, DET_EHIST_NBINS, DEFAULT_PACKET_LEN, RITE_CSP_ADDRESS

def read_chunk_v1(name, port, chunk_id):
    filename = name + "/" + "%03d-%04d-%06d" % (RITE_CSP_ADDRESS, port, chunk_id,)
    chunk_dta = None
    
    with open(filename, mode='rb') as file:
        chunk_dta = file.read()
    
    return chunk_dta
 
def read_chunk_v2(chunk_id, indexed_chunks):
    from . import hexchunks
    
    try:
        hextext = indexed_chunks[str(chunk_id)]
    except KeyError:
        print('ERROR: could not find chunk with chunk_id:', chunk_id)
        return None
    
    return hexchunks.read_chunk(hextext)

def packet_get_type(chunk_dta):
    return struct.unpack("<c", chunk_dta[:1])[0].decode("utf-8")

class metadada_processor():
    
    def __init__(self):
        self._verbose = False
        self.reset()
    
    def reset(self):
        self._packet = {}
        
    @property
    def verbose(self):
        return self._verbose
    
    @verbose.setter
    def verbose(self, v):
        self._verbose = v 
        
    def process(self, chunk_dta):
        (pck_type, outputform, start_nb, exp_nb, meas_mode, det_exposure, det_bias, det_threshold, en_hist_lim) = struct.unpack("<cHHHBdddH", chunk_dta[:34])
        self._packet['pck_type'] = pck_type
        self._packet['outputform'] = outputform
        self._packet['start_nb'] = start_nb
        self._packet['exp_nb'] = exp_nb
        self._packet['meas_mode'] = meas_mode
        self._packet['det_exposure'] = det_exposure
        self._packet['det_bias'] = det_bias
        self._packet['det_threshold'] = det_threshold
        self._packet['en_hist_lim'] = en_hist_lim
        (first_chunk, last_chunk) = struct.unpack("<II", chunk_dta[-16:-8])
        self._packet['first_chunk'] = first_chunk
        self._packet['last_chunk'] = last_chunk
        (nFrames, frame_id) = struct.unpack("<HH", chunk_dta[74:78])
        self._packet['n_frames'] = nFrames
        self._packet['frame_id'] = frame_id
        ts_epoch = struct.unpack("<I", chunk_dta[-8:-4])[0]
        ts_nsec = struct.unpack("<I", chunk_dta[-4:])[0]
        self._packet['ts_sec'] = ts_epoch
        self._packet['ts_nsec'] = ts_nsec        
        (filtering, scan_mode, count_roi, count_threshold) =  struct.unpack("<BBBL", chunk_dta[78:85])
        self._packet['filtering'] = filtering
        self._packet['scan_mode'] = scan_mode
        self._packet['count_roi'] = count_roi
        self._packet['count_threshold'] = count_threshold
        (min_val_frame, max_val_frame, min_val_filt, max_val_filt) =  struct.unpack("<HHHH", chunk_dta[85:93])
        self._packet['min_val_frame'] = min_val_frame
        self._packet['max_val_frame'] = max_val_frame
        self._packet['min_val_filt'] = min_val_filt
        self._packet['max_val_filt'] = max_val_filt
        self._packet['rois'] = [None]*DET_NROIS
        self._packet['frame_events'] = [None]*DET_NROIS
        self._packet['frame_events_filt'] = [None]*DET_NROIS
        self._packet['roi_sum'] = [None]*DET_NROIS
        self._packet['roi_sum_filt'] = [None]*DET_NROIS
        for roi_id in range(DET_NROIS):
            sz = 4 * 2
            offset = 34 + roi_id * sz
            (roi_row, roi_col, roi_height, roi_width) = struct.unpack("<HHHH", chunk_dta[offset:(offset+sz)])
            self._packet['rois'][roi_id] = (roi_row, roi_col, roi_height, roi_width)
            offset = 93 + 0*DET_NROIS * 2 + roi_id * 2
            frame_events = struct.unpack("<H", chunk_dta[offset:(offset+2)])[0]
            self._packet['frame_events'][roi_id] = frame_events
            offset = 93 + 1*DET_NROIS * 2 + roi_id * 2
            frame_events_filt = struct.unpack("<H", chunk_dta[offset:(offset+2)])[0]
            self._packet['frame_events_filt'][roi_id] = frame_events_filt
            offset = 93 + 2*DET_NROIS * 2 + 0*DET_NROIS * 4 + roi_id * 4
            roi_sum = struct.unpack("<L", chunk_dta[offset:(offset+4)])[0]
            self._packet['roi_sum'][roi_id] = roi_sum
            offset = 93 + 2*DET_NROIS * 2 + 1*DET_NROIS * 4 + roi_id * 4
            roi_sum_filt = struct.unpack("<L", chunk_dta[offset:(offset+4)])[0]
            self._packet['roi_sum_filt'][roi_id] = roi_sum_filt
        return self._packet

def is_dst(dt):
    """Determine whether or not Daylight Savings Time (DST)
    is in effect for a given date and time"""

    # Jan 1 of the relevant year
    x = datetime.datetime(dt.year, 1, 1, 1, 0, 0, 0, tzinfo=pytz.timezone(dt.tzinfo.zone))
    x_offset = ((x.utcoffset().seconds + 1800) // 3600) * 3600
    dt_offset = ((dt.utcoffset().seconds + 1800) // 3600) * 3600

    # if DST is in effect, their offsets will be different
    return not (x_offset == dt_offset)

def print_metadata(info):
    print("[RITE] start_nb: %u" % info['start_nb'])
    print("[MEAS] exp_nb: %u" % info['exp_nb'])
    print("[MEAS] frame_id: 0x%04x" % info['frame_id'])
    loc_dt = datetime.datetime.fromtimestamp(info['ts_sec'],tz=pytz.timezone('UTC'))
    #loc_dt = pytz.timezone('Europe/Prague').localize(loc_dt)
    #tz_text = 'CEST' if is_dst(loc_dt) else 'CET'
    tz_text = 'UTC'
    print("[MEAS] time: %s.%03d %s" % (loc_dt.strftime('%Y-%m-%d %H:%M:%S'), info['ts_nsec']/10**6, tz_text,))
    print("[DATA] outputForm: %u" % info['outputform'])
    print("[DATA] firstChunkId: %u" % info['first_chunk'])
    print("[DATA] lastChunkId: %u" % info['last_chunk'])
    print("[DET] mode: %u" % info['meas_mode'])
    print("[DET] scan mode, (count) threshold, roi: %u, %u, %u" % (info['scan_mode'], info['count_threshold'], info['count_roi'],))
    print("[DET] frameTime: %g (sec)" % info['det_exposure'])
    print("[DET] bias: %g (V)" % info['det_bias'])
    print("[DET] threshold: %g (keV)" % info['det_threshold'])
    print("[DET] nFrames: %u" % info['n_frames'])
    print("[PROC] filtering: %u" % info['filtering'])
    print("[PROC] (frame) min, max, (filtered) min, max: %10u %10u %10u %10u" % (info['min_val_frame'], info['max_val_frame'], info['min_val_filt'], info['max_val_filt'],))
    print("[PROC]    id  row  col  hgt  wdh  frame-evt   filt-evt  frame-sum   filt-sum")
    for roi_id in range(DET_NROIS):
        (roi_row, roi_col, roi_height, roi_width) = info['rois'][roi_id]
        frame_events = info['frame_events'][roi_id]
        frame_events_filt = info['frame_events_filt'][roi_id]
        roi_sum = info['roi_sum'][roi_id]
        roi_sum_filt = info['roi_sum_filt'][roi_id]
        print("[PROC] roi%ld: %4u %4u %4u %4u %10u %10u %10u %10u" % (roi_id, roi_row, roi_col, roi_height, roi_width,
                                                                      frame_events, frame_events_filt, roi_sum, roi_sum_filt,))

class binning_chunk_processor():
    
    def __init__(self, pck_type, bin_rows, bin_cols, height = DET_HEIGHT, width = DET_WIDTH):
        self.height = height // bin_rows
        self.width = width // bin_cols
        self.pck_type = pck_type
        self._verbose = False
        self.reset()

    def reset(self):
        self.pck_next_first_point = 0
        self._frame_id = None
        self._data = None
        
    @property
    def verbose(self):
        return self._verbose
    
    @verbose.setter
    def verbose(self, v):
        self._verbose = v

    @property
    def frame_id(self):
        return self._frame_id

    def process(self, chunk_dta):
        
            # packet length
            pck_len = len(chunk_dta)
            if(self._verbose):
                print("packet len: %u" % pck_len)

            # packet type
            (pck_type,) = struct.unpack("<c", chunk_dta[:1])
            pck_type = pck_type.decode("utf-8")
            if(self._verbose):
                print("packet type: %c" % (pck_type,))
            
            if(pck_type != self.pck_type):
                print("warning: wrong packet type: %c" % pck_type)
                return

            # frame id, packet_nb
            (frame_id,pck_nb) = struct.unpack("<HB", chunk_dta[1:4])
            if(self._verbose):
                print("frame_id: %u, pck_nb: %u" % (frame_id, pck_nb,))

            self._frame_id = frame_id

            pck_points = ((pck_len) - 4) // 2

            if (pck_nb==0):
                if(pck_points>0):
                    self.pck_next_first_point = 0
                    self._data = np.zeros((self.height*self.width,), np.int)
                else:
                    self._data = None

            if(pck_points>0):
                #first = self.pck_next_first_point
                first = pck_nb * (DEFAULT_PACKET_LEN - 4) // 2
                last = first + pck_points
                if(self._verbose):
                    print(first, last, struct.unpack("H"*pck_points, chunk_dta[4:]))
                self._data[first:last] = struct.unpack("H"*pck_points, chunk_dta[4:])
                self.pck_next_first_point += pck_points
       
    @property
    def data(self):
        return self._data.reshape((self.width,self.height)).copy() if self._data is not None else None

class proj_histogram_processor():
    
    def __init__(self, pck_type, roi):
        self.roi = roi
        self.pck_type = pck_type
        self._verbose = False
        self.reset()

    def reset(self):
        self.length = 0
        self.pck_next_first_point = 0
        self._frame_id = None
        self._data = None
        
    @property
    def verbose(self):
        return self._verbose
    
    @verbose.setter
    def verbose(self, v):
        self._verbose = v 

    @property
    def frame_id(self):
        return self._frame_id

    def process(self, chunk_dta):
        
            # packet length
            pck_len = len(chunk_dta)
            if(self._verbose):
                print("packet len: %u" % pck_len)

            # packet type
            (pck_type,) = struct.unpack("<c", chunk_dta[:1])
            pck_type = pck_type.decode("utf-8")
            if(self._verbose):
                print("packet type: %c" % (pck_type,))
            
            if(pck_type != self.pck_type):
                print("error: wrong packet type: %c, expected: %c" % (pck_type, self.pck_type,))
                return

            # frame id, packet_nb
            (frame_id,pck_nb,roi_id,length) = struct.unpack("<HBBH", chunk_dta[1:7])
            if(self._verbose):
                print("frame_id: %u, pck_nb: %u, roi_id: %d, length: %d" % (frame_id, pck_nb,roi_id,length,))

            self._frame_id = frame_id

            if(roi_id != self.roi):
                print("error: wrong roi: %d, expected: %d" % (roi_id, self.roi,))
                return
            
            pck_points = ((pck_len) - 7)

            if (pck_nb==0):
                self.length = length
                self.pck_next_first_point = 0
                self._data = np.zeros((self.length,), np.int)
            elif (length!=self.length):
                print("error: wrong length: %d, expected: %d " % (length, self.length,))
                return

            if(pck_points>0):
                #first = self.pck_next_first_point
                first = pck_nb * (DEFAULT_PACKET_LEN-7)
                last = first + pck_points
                if(self._verbose):
                    print(first, last, struct.unpack("B"*pck_points, chunk_dta[7:]))
                self._data[first:last] = struct.unpack("B"*pck_points, chunk_dta[7:])
                self.pck_next_first_point += pck_points

    @property      
    def data(self):
        return self._data.copy() if self._data is not None else None

class energy_histogram_processor():
    
    def __init__(self, pck_type, roi):
        self.roi = roi
        self.pck_type = pck_type
        self._verbose = False
        self.reset()

    def reset(self):
        self.length = 0
        self.en_hist_lim = 0
        self.pck_next_first_point = 0
        self._frame_id = None
        self._data = None
        
    @property
    def verbose(self):
        return self._verbose
    
    @verbose.setter
    def verbose(self, v):
        self._verbose = v 

    @property
    def frame_id(self):
        return self._frame_id

    def process(self, chunk_dta):
        
            # packet length
            pck_len = len(chunk_dta)
            if(self._verbose):
                print("packet len: %u" % pck_len)

            # packet type
            (pck_type,) = struct.unpack("<c", chunk_dta[:1])
            pck_type = pck_type.decode("utf-8")
            if(self._verbose):
                print("packet type: %c" % (pck_type,))
            
            if(pck_type != self.pck_type):
                print("error: wrong packet type: %c, expected: %c" % (pck_type, self.pck_type,))
                return

            # frame id, packet_nb
            (frame_id,pck_nb,roi_id,en_hist_lim) = struct.unpack("<HBBH", chunk_dta[1:7])
            if(self._verbose):
                print("frame_id: %u, pck_nb: %u, roi_id: %d, en_hist_lim: %d" % (frame_id, pck_nb,roi_id,en_hist_lim,))

            self._frame_id = frame_id
            self.en_hist_lim = en_hist_lim

            if(roi_id != self.roi):
                print("error: wrong roi: %d, expected: %d" % (roi_id, self.roi,))
                return
            
            pck_points = ((pck_len) - 7) // 2

            if (pck_nb==0):
                self.length = DET_EHIST_NBINS
                self.pck_next_first_point = 0
                self._data = np.zeros((self.length,), np.int)
            elif (length!=self.length):
                print("error: wrong length: %d, expected: %d " % (length, self.length,))
                return

            if(pck_points>0):
                #first = self.pck_next_first_point
                first = pck_nb * ((DEFAULT_PACKET_LEN-7) // 2)
                last = first + pck_points
                if(self._verbose):
                    print(first, last, struct.unpack("H"*pck_points, chunk_dta[7:]))
                self._data[first:last] = struct.unpack("H"*pck_points, chunk_dta[7:])
                self.pck_next_first_point += pck_points
    
    @property
    def data(self):
        return (self._data.copy(),self.en_hist_lim) if self._data is not None else (None,None)


class framestream_processor():
    
    def __init__(self, pck_type, height = DET_HEIGHT, width = DET_WIDTH):
        self.pck_type = pck_type
        self.width = width
        self.height = height
        self._verbose = False
        self.reset()
    
    def reset(self):
        self._nevents = 0
        self._frame_id = None
        self._data = np.zeros((self.height*self.width,), np.int)
        
    @property
    def verbose(self):
        return self._verbose
    
    @verbose.setter
    def verbose(self, v):
        self._verbose = v 

    @property
    def frame_id(self):
        return self._frame_id

    def process(self, chunk_dta):
        
            # packet length
            pck_len = len(chunk_dta)
            if(self._verbose):
                print("packet len: %u" % pck_len)

            # packet type
            (pck_type,) = struct.unpack("<c", chunk_dta[:1])
            pck_type = pck_type.decode("utf-8")
            if(self._verbose):
                print("packet type: %c" % (pck_type,))
            
            if(pck_type != self.pck_type):
                print("error: wrong packet type: %c, expected: %c" % (pck_type, self.pck_type,))
                return

            # frame id, packet_nb
            (frame_id,pixelsInPacket) = struct.unpack("<HB", chunk_dta[1:4])
            if(self._verbose):
                print("frame_id: %u, pixelsInPacket: %d" % (frame_id, pixelsInPacket,))

            self._frame_id = frame_id

            pck_points = ((pck_len) - 4) // 4
            
            if(pck_points != pixelsInPacket):
                print("warning: wrong pixels in packet: %d, expected: %d" % (pck_points, pixelsInPacket,))

            #if (pck_nb==0):
            #    self.nevents = 0
            #    self._data = np.zeros((height*width,), np.int)

            if(pck_points>0):
                # read data
                raw = np.array(struct.unpack("HH"*pck_points, chunk_dta[4:]))
                idx = raw[0::2]
                val = raw[1::2]
                self._data[idx] = val
                self._nevents += pck_points

    @property
    def nevents(self):
        return self._nevents
    
    @property
    def data(self):
        return self._data.reshape(self.height,self.width).copy() if (self.nevents>0) else None

class datastream_processor:
    
    def __init__(self, height = DET_HEIGHT, width = DET_WIDTH):
        self.bin8_processor  = binning_chunk_processor('D',  8,  8, height, width)
        self.bin16_processor = binning_chunk_processor('E', 16, 16, height, width)
        self.bin32_processor = binning_chunk_processor('F', 32, 32, height, width)

        self.e_processors = [None]*DET_NROIS
        self.h_processors = [None]*DET_NROIS
        self.H_processors = [None]*DET_NROIS

        for iroi in range(DET_NROIS):
            self.e_processors[iroi] = energy_histogram_processor('e', iroi)
            self.h_processors[iroi] = proj_histogram_processor('h', iroi)
            self.H_processors[iroi] = proj_histogram_processor('H', iroi)

        self.filt_processor = framestream_processor('B', height, width)
        self.frame_processor = framestream_processor('b', height, width)
        
        self.reset()
    
    def process(self, chunk_dta):
        
        pck_type = packet_get_type(chunk_dta)
        
        if(pck_type=='e'):
            (roi_id,) = struct.unpack("<B", chunk_dta[4:5])
            self.e_processors[roi_id].process(chunk_dta)
        elif(pck_type=='F'):
            self.bin32_processor.process(chunk_dta)
        elif(pck_type=='E'):
            self.bin16_processor.process(chunk_dta)
        elif(pck_type=='D'):
            self.bin8_processor.process(chunk_dta)
        elif(pck_type=='h'):
            (roi_id,) = struct.unpack("<B", chunk_dta[4:5])
            self.h_processors[roi_id].process(chunk_dta)
        elif(pck_type=='H'):
            (roi_id,) = struct.unpack("<B", chunk_dta[4:5])
            self.H_processors[roi_id].process(chunk_dta)
        elif(pck_type=='B'):
            self.filt_processor.process(chunk_dta)
        elif(pck_type=='b'):
            self.frame_processor.process(chunk_dta)


    def reset(self):
        self.bin8_processor.reset()
        self.bin16_processor.reset()
        self.bin32_processor.reset()
        for iroi in range(DET_NROIS):
            self.e_processors[iroi].reset()
            self.h_processors[iroi].reset()
            self.H_processors[iroi].reset()
        self.filt_processor.reset()
        self.frame_processor.reset()
        self._data = {'frame': None, 'filt': None, 'ehist': [None]*DET_NROIS, \
                      'row_proj': [None]*DET_NROIS, 'col_proj': [None]*DET_NROIS, \
                      'bin8': None, 'bin16': None, 'bin32': None}
    
    @property
    def data(self):
        self._data['frame'] = self.frame_processor.data
        self._data['filt'] = self.filt_processor.data
        self._data['bin8'] = self.bin8_processor.data
        self._data['bin16'] = self.bin16_processor.data
        self._data['bin32'] = self.bin32_processor.data
        for iroi in range(DET_NROIS):
            self._data['ehist'][iroi] = self.e_processors[iroi].data
            self._data['row_proj'][iroi] = self.h_processors[iroi].data
            self._data['col_proj'][iroi] = self.H_processors[iroi].data
        return self._data

    def print_info(self):
        print("[DATA] frame: ", None if self.frame_processor.data is None else self.frame_processor.data.shape)
        print("[DATA] filt: ", None if self.filt_processor.data is None else self.filt_processor.data.shape)
        out = []
        for iroi in range(DET_NROIS):
            out = out + [None if self.H_processors[iroi].data is None else self.H_processors[iroi].data.shape,]
        print("[DATA] col_proj: ", out)
        out = []
        for iroi in range(DET_NROIS):
            out = out + [None if self.h_processors[iroi].data is None else self.h_processors[iroi].data.shape,]
        print("[DATA] row_proj: ", out)
        print("[DATA] bin8: ", None if self.bin8_processor.data is None else self.bin8_processor.data.shape)
        print("[DATA] bin16: ", None if self.bin16_processor.data is None else self.bin16_processor.data.shape)
        print("[DATA] bin32: ", None if self.bin32_processor.data is None else self.bin32_processor.data.shape)
        out = []
        for iroi in range(DET_NROIS):
            out = out + [(None,None) if self.e_processors[iroi].data[0] is None else (self.e_processors[iroi].data[0].shape,self.e_processors[iroi].data[1])]
        print("[DATA] ehist: ", out)

class experiment_config_processor():

    def __init__(self):
        self._verbose = False
        self.reset()

    def reset(self):
        self._packet = {}

    @property
    def verbose(self):
        return self._verbose

    @verbose.setter
    def verbose(self, v):
        self._verbose = v

    def process(self, chunk_dta):
        (pck_type, valid, automeasure, turnoff, meas_mode, filtering, scan_mode, count_roi) = struct.unpack("<cBBBBBBB", chunk_dta[:8])
        self._packet['pck_type'] = pck_type.decode('utf-8')
        self._packet['valid'] = valid
        self._packet['automeasure'] = automeasure
        self._packet['turnoff'] = turnoff
        self._packet['meas_mode'] = meas_mode
        self._packet['filtering'] = filtering
        self._packet['scan_mode'] = scan_mode
        self._packet['count_roi'] = count_roi
        (det_threshold, det_bias, det_exposure, det_period) = struct.unpack("<dddd", chunk_dta[8:40])
        self._packet['det_threshold'] = det_threshold
        self._packet['det_bias'] = det_bias
        self._packet['det_exposure'] = det_exposure
        self._packet['det_period'] = det_period
        (n_frames, outputform, count_threshold, en_hist_lim) = struct.unpack("<HHIH", chunk_dta[40:50])
        self._packet['n_frames'] = n_frames
        self._packet['outputform'] = outputform
        self._packet['count_threshold'] = count_threshold
        self._packet['en_hist_lim'] = en_hist_lim
        self._packet['rois'] = [None]*DET_NROIS
        for roi_id in range(DET_NROIS):
            sz = 4 * 2
            offset = 50 + roi_id * sz
            (roi_row, roi_col, roi_height, roi_width) = struct.unpack("<HHHH", chunk_dta[offset:(offset+sz)])
            self._packet['rois'][roi_id] = (roi_row, roi_col, roi_height, roi_width)
        return self._packet