#!/usr/bin/env python
 
import numpy as np

def camera_binning_no_hot_no_gap(img, nbin):
    timg = np.zeros((img.shape[0]//nbin,img.shape[1]), img.dtype)
    for offset in range(nbin):
        timg += img[offset::nbin,:]
    oimg = np.zeros((img.shape[0]//nbin,img.shape[1]//nbin), img.dtype)
    for offset in range(nbin):
        oimg += timg[:,offset::nbin]
    from . import DET_DTYPE
    oimg[oimg>np.iinfo(DET_DTYPE).max] = np.iinfo(DET_DTYPE).max # overflow
    return oimg

def check_binned_data(bin_dta, img, nbin):
    a = bin_dta - camera_binning_no_hot_no_gap(img,nbin)
    if(np.any(a)):
        print("test failed: size=(%d,%d)" % (bin_dta.shape[0],bin_dta.shape[1],))
    return not np.any(a)

def quadratic_positive_root(a, b, c):
    pass
    
def tot2energy_formula(tot, a, b, c, t):
    aa = a
    bb = b - a*t - tot
    cc = t*tot - c - b*t
    en = np.zeros(tot.shape, np.float)*np.nan
    en1 = np.zeros(tot.shape, np.float)*np.nan
    en2 = np.zeros(tot.shape, np.float)*np.nan
    # tot==0
    lidx = tot==0
    en[lidx] = 0
    # aa==0
    lidx = np.logical_and(tot>0, aa == 0.0, bb != 0.0)
    en[lidx] = -cc[lidx]/bb[lidx]
    # bb==0
    lidx = (tot>0)*(bb == 0.0)*(aa != 0.0)*(cc <= 0.0)
    en[lidx] = np.sqrt(-cc[lidx])/np.abs(aa[lidx])
    # aa<>0, bb<>0, determinant>=0.0
    q = bb*bb - 4*aa*cc
    lidx = (tot>0)*(aa != 0.0)*(b != 0.0)*(q >= 0.0)
    q[lidx] = -0.5*(bb[lidx] + np.sign(bb[lidx])*np.sqrt(q[lidx]))
    lidxp = np.zeros(lidx.shape, np.bool)
    lidxp[lidx] = q[lidx]/aa[lidx] >= 0.0
    en1[lidxp] = q[lidxp]/aa[lidxp]
    lidxp = np.zeros(lidx.shape, np.bool)
    lidxp[lidx] = cc[lidx]/q[lidx] >= 0.0
    en2[lidxp] = cc[lidxp]/q[lidxp]
    # choose larger from en1 and en2
    lidxp = lidx*(en1>=0.0)*np.isnan(en2)
    en[lidxp] = en1[lidxp]
    lidxp = lidx*np.isnan(en1)*(en2>=0.0)
    en[lidxp] = en2[lidxp]
    lidxp = lidx*(en1>=en2)
    en[lidxp] = en1[lidxp]
    lidxp = lidx*(en2>en1)
    en[lidxp] = en2[lidxp]
    return en

def tot2energy(tot, det_id='J08-W0281'):
    import os
    det_configs_dir = os.path.join(os.path.dirname(__file__), 'detector_configs')
    ca = np.loadtxt(os.path.join(det_configs_dir, 'config_' + det_id, 'caliba.txt'))
    cb = np.loadtxt(os.path.join(det_configs_dir, 'config_' + det_id, 'calibb.txt'))
    cc = np.loadtxt(os.path.join(det_configs_dir, 'config_' + det_id, 'calibc.txt'))
    ct = np.loadtxt(os.path.join(det_configs_dir, 'config_' + det_id, 'calibt.txt'))
    en = tot2energy_formula(tot, ca, cb, cc, ct)
    en[ca==0] = np.nan # we do not trust pixels with ca==0
    return en

def calc_histogram(data, en_max=0, nbins=0):
    if(en_max==0):
        en_max = np.max(data)
    if(nbins==0):
        from . import DET_EHIST_NBINS
        nbins = DET_EHIST_NBINS

    # true zero cases
    ntruezeros = np.count_nonzero(data==0)
    # special case
    if(np.all(data==0)):
        edges = np.arange(nbins)*(en_max/nbins)
        frq = np.zeros(edges.shape, np.int)
        
        return frq, edges
    
    if(np.any(data==en_max) and np.all(data<=en_max)):
        frq, edges = np.histogram(data,nbins)
    else:
        # a trik to restrict histogram range: calc first bin manually and set high values to 0
        nlow = np.count_nonzero(data<(en_max/nbins))
        img = data.copy()
        img[img>en_max] = 0
        # a trick to enforce a histogram range
        idx = np.argwhere(img==0)
        if(idx.size>0):
            img[idx[0][0],idx[0][1]] = en_max
        frq, edges = np.histogram(img,nbins)
        frq[0] = nlow
        if(idx.size>0):
            frq[-1] -= 1
            
    frq[0] -= ntruezeros
    
    return frq, edges
