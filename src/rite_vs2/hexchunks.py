from glob import glob
from datetime import datetime, timedelta
import os, re
from collections import OrderedDict
import hexdump

from . import RITE_CSP_ADDRESS

def read_chunk(hextext):
    # starts with the 2nd line
    start = hextext.find('\n') + 1
    # convert to bytes
    # ugly trick to workaround strange hexdump.restore error ... remove |....|
    _hextext = hextext[start:].rstrip()
    _t = _hextext.split('\n')
    for iline, line in enumerate(_t):
        _t[iline] = line[:line.find('|')].rstrip()
    _hextext = '\n'.join(_t)
    b = hexdump.restore(_hextext)
    # get DK id and time from the header
    dk_id = int(hextext[:start].split('id=')[-1].split(',')[0])
    dk_time = hextext[:start].split('time=')[-1].split(',')[0]
    return b, dk_id, dk_time

def chunk_ids(chunks):
    
    ids = [None]*len(chunks)
    
    for i, chunk in enumerate(chunks):
        # get header in the first line
        h_end = chunk.find('\n') + 1
        # get chunk id
        ids[i] = int(chunk[:h_end].split('id=')[-1].split(',')[0])

    return ids
        
def index_chunks_by_id(chunks):
    
    indexed = OrderedDict()
    
    for chunk in chunks:
        # get header in the first line
        h_end = chunk.find('\n') + 1
        # get chunk id
        dk_id_s = chunk[:h_end].split('id=')[-1].split(',')[0]
        # save chunk to dictionary
        indexed[dk_id_s] = chunk
        
    return indexed
   
def find_chunks(slot=None, port=0, date_start=None, date_end=None, chunk_id=None, data_path=None):
    
    # sanitize parameters
    if chunk_id is not None:
        chunk_id = str(chunk_id)
    else:
        chunk_id = ''

    if date_start is None:
        date_start = '2022-01-01'
    
    if type(date_start) == str:
        date_start = re.sub(r'[^\w]', '', date_start)
    
    if date_end is None:
        date_start = '2032-12-31'

    if type(date_end) == str:
        date_end = re.sub(r'[^\w]', '', date_end)
    
    if date_start is not None:
        date_start = str(date_start)
    
    if date_end is not None:
        date_end = str(date_end)

    if data_path is None:
        data_path = os.getenv('RITE_DATA_DIR')
        if data_path is None:
            raise ValueError('ERROR: data_path argument cannot be None or env variable RITE_DATA_DIR must be defined.')
    
    if slot is None:
        slot = RITE_CSP_ADDRESS
           
    chunkStringList = []
    chunkFilesList=[]
    chunks=[]
    chunk = []
    chunkId = ("id=" + str(chunk_id))
    chunk_stop = "chunk"  # Just a helping variable to find the end of the chunk Data


    date_start = datetime(year=int(date_start[0:4]), month=int(date_start[4:6]), day=int(date_start[6:8]))
    date_end = datetime(year=int(date_end[0:4]), month=int(date_end[4:6]), day=int(date_end[6:8]))
    delta = date_end - date_start
    deltaDays = delta.days

    days = []

    for i in range(0, deltaDays + 1):
        date = date_start + timedelta(days=i)
        date = date.strftime("%Y%m%d")
        date = (date[0:4] + "-" + date[4:6] + "-" + date[6:8])
        days.append(date)


    files = glob(os.path.join(data_path,"*S"+str(slot)+"P"+str(port)+"*"))  # Find a path to all the files containing given string

    for path in files:
        fileName = os.path.split(path)[-1]  # Gets a name of the file

        file = open(path)
        content = file.readlines()  # Read the whole content of a file
        file.close()

        for line in content:
            # identify a header line
            if (('Data chunk id=' in str(line)) and ('time=' in str(line))):
                if chunkId in str(line): # Comparing every line in a file with given desired values of a chunk
                    # filter by specific chunkId if supplied
                    if len(chunkId)>3:
                        try:
                            if not line.split('id=')[-1].split(',')[0]==chunkId[3:]:
                                continue
                        except:
                            print('WARNING: strange header line (%s): %s' % (path, line,))
                            continue
                    # filter by date
                    try:
                        if line.split('time=')[-1].split(' ')[0] not in days:
                            continue
                    except:
                            print('WARNING: strange header line (%s): %s' % (path, line,))
                            continue
                    # passed filter, add to output
                    chunk.append(fileName) # Saving the name of a file with desired chunk
                    index = content.index(line)
                    for i in range (0,len(content)+1): # Saving the chunk from its header till the beginning of a new chung
                        chunk.append(content[index])
                        index=index+1
                        try:
                            if chunk_stop in content[index]: # Breaking when it finds the beginning of a new chunk
                                chunks.append(chunk)
                                chunk=[]
                                break
                        except IndexError:
                            # this is the last chunk in the file
                            chunks.append(chunk)
                            chunk=[]
                            break

    for item in chunks:
        chunkString = "".join(item[1:])
        chunkFilesList.append(item[0])
        chunkStringList.append(chunkString)

    return chunkStringList, chunkFilesList
